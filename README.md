# OpenML dataset: Code_Smells_Data_Class

https://www.openml.org/d/43079

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset combines records from the MLCQ dataset with metrics extracted using the PMD Tool and the Understand tool, to determine whether a file contains code smells. Please note that the records are on (sub)class level. Classification task, the default class (severity) should be binarized with a static threshold (preferably between 0.5 and 2.5). Please carefully read the publication to understand how to use this dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43079) of an [OpenML dataset](https://www.openml.org/d/43079). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43079/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43079/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43079/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

